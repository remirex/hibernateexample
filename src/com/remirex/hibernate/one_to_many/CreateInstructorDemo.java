package com.remirex.hibernate.one_to_many;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateInstructorDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // create object
            Instructor instructor = new Instructor("John", "Doe", "john@example.com");
            InstructorDetail instructorDetail = new InstructorDetail("http://www.john.com/youtube", "guitar");

            // associate the object
            instructor.setInstructorDetail(instructorDetail);

            // start transaction
            session.beginTransaction();

            // save object
            System.out.println("Saving instructor ..." + instructor);
            session.save(instructor);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

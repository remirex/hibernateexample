package com.remirex.hibernate.one_to_one_bi;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // get instructor
            int instructorId = 2;

            // start transaction
            session.beginTransaction();

            Instructor instructor = session.get(Instructor.class, instructorId);

            System.out.println("\nInstructor: " + instructor);

            // save the object, note: this will save the details object because of CascadeType.ALL !!!
            System.out.println("\nDeleting: " + instructor);
            session.delete(instructor);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}

package com.remirex.hibernate.one_to_one_bi;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // create the object
            Instructor instructor = new Instructor("Test3", "Test3", "test3@email.com");
            InstructorDetail instructorDetail = new InstructorDetail("http://www.test3.com/youtube", "test3 hobby");

            // associate the objects
            instructor.setInstructorDetail(instructorDetail);

            // start transaction
            session.beginTransaction();

            // save the object, note: this will save the details object because of CascadeType.ALL !!!
            System.out.println("Saving the object: " + instructor);
            session.save(instructor);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}

package com.remirex.hibernate.one_to_one_bi;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteInstructorDetailDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // get object
            int instructorDetailId = 3;
            // start transaction
            session.beginTransaction();

            InstructorDetail instructorDetail = session.get(InstructorDetail.class, instructorDetailId);

            // print detail
            System.out.println("\nInstructor detail: " + instructorDetail);
            System.out.println("Instructor with associated detail: " + instructorDetail.getInstructor());

            // delete detail
            System.out.println("Deleting detail " + instructorDetail);

            // remove the associated object reference
            // break bi-directional
            instructorDetail.getInstructor().setInstructorDetail(null);
            session.delete(instructorDetail);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}

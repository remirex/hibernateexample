package com.remirex.hibernate.one_to_one_bi;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GetInstructorDetailDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // get object
            int instructorId = 3;
            // start transaction
            session.beginTransaction();

            Instructor instructor = session.get(Instructor.class, instructorId);

            // print detail
            System.out.println("\nInstructor: " + instructor);
            System.out.println("Instructor detail: " + instructor.getInstructorDetail());

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}

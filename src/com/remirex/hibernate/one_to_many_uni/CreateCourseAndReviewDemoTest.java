package com.remirex.hibernate.one_to_many_uni;

import com.remirex.hibernate.SessionFactoryRule;
import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CreateCourseAndReviewDemoTest {

    public final SessionFactoryRule sessionFactoryRule = new SessionFactoryRule();

    @Test
    public void createCourseAndRelatedReviews() {
        // create session
        Session session = sessionFactoryRule.createSession();
        Course tempCourse = createCourse();
        // start transaction
        sessionFactoryRule.beginTransaction();
        session.save(tempCourse);
        // commit transaction
        sessionFactoryRule.commit();
    }

    private Course createCourse() {
        // create course
        Course course = new Course();
        course.setTitle("Learn Spring Boot");

        // create review 1
        Review review = new Review("You are the best");
        course.addReview(review);

        // create review 2
        Review review2 = new Review("You are awesome!");
        course.addReview(review2);

        assertEquals("Learn Spring Boot", course.getTitle());

        assertEquals("You are the best", review.getComment());
        assertEquals("You are awesome!", review2.getComment());

        List<Review> reviews = course.getReviews();
        assertEquals(2, reviews.size());

        return course;
    }

}
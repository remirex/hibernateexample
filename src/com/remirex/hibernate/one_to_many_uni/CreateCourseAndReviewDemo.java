package com.remirex.hibernate.one_to_many_uni;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateCourseAndReviewDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // create course
            Course course = new Course("Test new course");

            // add some reviews
            course.addReview(new Review("Test comment1"));
            course.addReview(new Review("Test comment2"));
            course.addReview(new Review("Test comment3"));
            // start transaction
            session.beginTransaction();

            // save course
            session.save(course);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

package com.remirex.hibernate.many_to_many;

import com.remirex.hibernate.SessionFactoryRule;
import com.remirex.hibernate.entity.Course;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeleteCourseFromDatabaseDemoTest {

    public SessionFactoryRule sessionFactoryRule = new SessionFactoryRule();

    Session session = sessionFactoryRule.createSession();

    @Test
    public void deleteCourse() {
        Course tempCourse = findCourse();
        session.delete(tempCourse);
        sessionFactoryRule.commit();
    }

    private Course findCourse() {
        int courseId = 1;
        // start transaction
        sessionFactoryRule.beginTransaction();
        Course course = session.get(Course.class, courseId);
        if (course == null) {
            System.out.println(courseId + " Does not exist in db");
        }
        assertEquals(courseId, course.getId());

        return course;
    }

}
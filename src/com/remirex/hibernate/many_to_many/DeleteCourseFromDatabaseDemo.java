package com.remirex.hibernate.many_to_many;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteCourseFromDatabaseDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            int courseId = 5;
            // start transaction
            session.beginTransaction();

            // get course
            Course course = session.get(Course.class, courseId);
            System.out.println("Course: " + courseId);

            // delete course
            session.delete(course);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

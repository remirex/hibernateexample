package com.remirex.hibernate.many_to_many;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateCourseAndStudentDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // create course
            Course course = new Course("Master class Hibernate");

            // start transaction
            session.beginTransaction();

            // save course
            System.out.println("Saving the course " + course);
            session.save(course);

            // create students
            Student student = new Student("New student", "New student", "new@example.com");
            Student student2 = new Student("New student2", "New student2", "new2@example.com");

            // add student to the course
            course.addStudent(student);
            course.addStudent(student2);

            // save student
            System.out.println("Saving students ...");
            session.save(student);
            session.save(student2);
            System.out.println("Saved students: " + course.getStudents());

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

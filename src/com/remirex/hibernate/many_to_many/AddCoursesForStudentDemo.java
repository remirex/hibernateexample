package com.remirex.hibernate.many_to_many;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AddCoursesForStudentDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            int studentId = 5;
            // start transaction
            session.beginTransaction();

            // get student
            Student student = session.get(Student.class, studentId);
            System.out.println("Student: " + student);

            // create courses
            Course course = new Course("Learn Laravel 6.0");
            Course course2 = new Course("Learn Java");

            // add student to courses
            course.addStudent(student);
            course2.addStudent(student);

            // save courses
            session.save(course);
            session.save(course2);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

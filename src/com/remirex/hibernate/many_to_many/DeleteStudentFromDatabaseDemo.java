package com.remirex.hibernate.many_to_many;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteStudentFromDatabaseDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            int studentId = 5;
            // start transaction
            session.beginTransaction();

            // get student
            Student student = session.get(Student.class, studentId);
            System.out.println("Student: " + student);

            // delete student
            session.delete(student);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");


        } finally {
            // clean up the code
            session.close();
            factory.close();
        }
    }
}

package com.remirex.hibernate.demo;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteStudentDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // start transaction
            session.beginTransaction();

            // get student
            int studentId = 3;
            Student theStudent = session.get(Student.class, studentId);
            System.out.println("\nStudent: " + theStudent);

            // delete student
            System.out.println("\nDeleting student ...");
            session.delete(theStudent);

            System.out.println("======================");

            // HQL query
            System.out.println("Example: delete student where id = 5");
            session.createQuery("delete from Student s where s.id=5").executeUpdate();

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}

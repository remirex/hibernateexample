package com.remirex.hibernate.demo;

import com.remirex.hibernate.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UpdateStudentDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // start transaction
            session.beginTransaction();

            // get student
            int studentId = 1;
            Student theStudent = session.get(Student.class, studentId);
            System.out.println("\nStudent: " + theStudent);

            // update student
            System.out.println("\nUpdating student ...");
            theStudent.setFirstName("Updated First Name");
            theStudent.setLastName("Updated Last Name");
            theStudent.setEmail("Updated email");

            System.out.println("\nUpdated student: " + theStudent);

            System.out.println("================================");

            // HQL query
            System.out.println("Example: update email for all student");
            session.createQuery("update Student s set s.email='hibernate@gmail.com'").executeUpdate();

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        } finally {
            // clean up code
            session.close();
            factory.close();
        }
    }
}
